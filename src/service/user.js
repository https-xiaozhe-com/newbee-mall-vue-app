/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除我方将保留所有法律责任追究！
 * 本系统已申请软件著作权，受国家版权局知识产权以及国家计算机软件著作权保护！
 * 可正常分享和学习源码，不得用于违法犯罪活动，违者必究！
 * Copyright (c) 2020 陈尼克 all rights reserved.
 * 版权所有，侵权必究！
 */

import axios from '../utils/axios'

// 请求用户的详细信息
export function getUserInfo() {
    return axios.get('/user/info');
}
// 修改用户的详细信息
export function EditUserInfo(params) {
    return axios.put('/user/info', params);
}
// 登录方法
export function login(params) {
    return axios.post('/user/login', params);
}
// 退出登录的方法
export function logout() {
    return axios.post('/user/logout')
}
// 注册用户的详细信息
export function register(params) {
    return axios.post('/user/register', params);
}