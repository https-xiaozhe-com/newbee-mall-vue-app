/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除我方将保留所有法律责任追究！
 * 本系统已申请软件著作权，受国家版权局知识产权以及国家计算机软件著作权保护！
 * 可正常分享和学习源码，不得用于违法犯罪活动，违者必究！
 * Copyright (c) 2020 陈尼克 all rights reserved.
 * 版权所有，侵权必究！
 */
import axios from 'axios'

// 移动端ui组件库
import { Toast } from 'vant'

import router from '../router'

axios.defaults.baseURL = process.env.NODE_ENV == 'development' ? 'http://backend-api-01.newbee.ltd/api/v1' : 'http://backend-api-01.newbee.ltd/api/v1'
    // 跨域请求时 是否需要凭证
axios.defaults.withCredentials = true
axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'

// 用于判断用户是否登录
axios.defaults.headers['token'] = localStorage.getItem('token') || ''
    // 定义post中data的格式 ：1.form-data（图片上传，文件上传） 2.application/json 目前比较流行 默认格式
axios.defaults.headers.post['Content-Type'] = 'application/json'

// 响应拦截器 参数（请求成功，请求失败）
axios.interceptors.response.use(res => {
        if (typeof res.data !== 'object') {
            // vant中的文字提示  fail表示失败
            Toast.fail('服务端异常！')
            return Promise.reject(res)
        }
        if (res.data.resultCode != 200) {
            if (res.data.message) Toast.fail(res.data.message)
            if (res.data.resultCode == 416) {
                router.push({ path: '/login' })
            }
            return Promise.reject(res.data)
        }
        console.log(res.data);
        return res.data
    })
    // 配置响应拦截器  参数（响应成功，响应失败） 通过eject清除拦截器
const axiosRequest = axios.interceptors.request.use(() => {}, () => {})
    // 清除拦截器
axios.interceptors.request.eject(axiosRequest)

export default axios